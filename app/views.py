from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.utils.cache import patch_vary_headers
from django.shortcuts import render, redirect
from django.db.models import Q, Avg, Count, Min, Sum
from django.contrib.auth.decorators import user_passes_test
from django.contrib import messages
from datetime import datetime, time

from app.models import Estado, Proyecto, Tarea, Avance, Developer

def index(request):
    return render(request, 'app/index.html')

@login_required
def menu(request):

    pubs_tarea = Tarea.objects.filter(proyecto__productowner_id=request.user.id).aggregate(num_tarea=Sum('tiempoestimado'))['num_tarea'] or 0
    avances_restante = Avance.objects.aggregate(num_restante=Count('tiemporestante'))['num_restante'] or 0
    avances_trabajado = Avance.objects.aggregate(total_trabjadas=Sum('tiempotrabajado'))['total_trabjadas'] or 0

    contexto = {
            'lista_tarea': pubs_tarea,
            'lista_trabajado': avances_trabajado, 
            'lista_restante': avances_restante
        }
    return render(request, 'app/menu.html', contexto)

@login_required
def charts(request):
    return render(request, 'app/charts.html')

def forgotpassword(request):
    return render(request, 'app/forgot-password.html')

def register(request):
    return render(request, 'app/register.html')

def registrar(request):
    try:
        if request.method != 'POST':
            return render(request, 'app/404.html')

        # Recupera la info del formulario
        nombre = request.POST['FirstName']
        apellido = request.POST['LastName']
        email = request.POST['Email']
        username = request.POST['Username']
        password = request.POST['Password']
        repeatPassword = request.POST['RepeatPassword']
        rol = request.POST['CustomSelect']
        
        if password != repeatPassword:
            messages.warning(request, 'Credenciales no válidas')
            return render(request, 'app/register.html')

        emails = None
        usuario = None
        try:
            usuario = User.objects.get(username=username)
            emails = User.objects.get(email=email)
        except:
            pass

        if usuario is None and emails is None:
            # Crea el usuario
            usuario = User(
                username=username,
                first_name=nombre,
                last_name=apellido,
                email=email
            )
            usuario.set_password(password)
            # Guarda el usuario en BD
            usuario.save()
            messages.success(request, 'Se guardo correctamente.')

            return redirect('app:index')
        else:
            messages.warning(request, 'El usuario o email ya existe!')
            return render(request, 'app/register.html')
    except:
        return render(request, 'app/404.html')

@login_required
def seeProyect(request):
    try:
        # Obtiene las proyecto y user
        proyectos = Proyecto.objects.all()
        user = User.objects.all()
        #filtrar datos
        proyectos = proyectos.filter(productowner_id=request.user.id)
        # Crea el contexto
        contexto = {
            'lista_proyecto': proyectos,
            'lista_user': user,
            'active_menu':'active'        
        }
        return render(request, 'app/seeProyect.html', contexto)
    except:
        return render(request, 'app/404.html')

@login_required
def saveProyect(request):
    try:

        if request.method != 'POST':
            return render(request, 'app/404.html')

        # Recupera la info del formulario
        proyectName = request.POST['ProyectName']
        proyectDescrption = request.POST['ProyectDescrption']
        
        # Obtiene la User
        id_user = User.objects.get(id=request.user.id)
        # Crea el objeto
        proyecto = Proyecto(
            nombre=proyectName,
            descripcion=proyectDescrption,
            productowner=id_user
        )

        # Guarda el objeto en la BD
        proyecto.save()
        messages.success(request, 'Se guardo correctamente.')

        return seeProyect(request)
    except:
        return render(request, 'app/404.html')

@login_required
def addDeveloper(request):
    try:
        
        if request.method != 'POST':
            return render(request, 'app/404.html')

        # Recupera la info del formulario
        proyectName = request.POST['CustomSelectproyect']
        developerName = request.POST['CustomSelectdeveloper']

        # Obtiene la Proyecto y User
        id_proyecto = Proyecto.objects.get(id=proyectName)
        id_user = User.objects.get(id=developerName)

        #validaciones
        developers = Developer.objects.all()
        developers = developers.filter(
            Q(proyecto=id_proyecto) & Q(usuario=id_user)
        )

        if developers.exists():
            messages.warning(request, 'El developer ya fue asignado anteriormente al proyecto') 
            return listdeveloper(request)
            
        # Crea el objeto
        developer = Developer(
            proyecto = id_proyecto,
            usuario=id_user,
            activo=True
        )

        # Guarda el objeto en la BD
        developer.save()
        messages.success(request, 'Se guardo correctamente.')

        return listdeveloper(request)
    except:
        return render(request, 'app/404.html')

@login_required
def editProyect(request):
    try:
        
        if request.method != 'POST':
            return render(request, 'app/404.html')

        # Recupera la info del formulario
        idproyect = request.POST['prodId']
        proyectName = request.POST['ProyectName']
        proyectDescrption = request.POST['ProyectDescrption']
        
        # Obtiene la User
        proyecto = Proyecto.objects.get(id=idproyect)
        # Crea el objeto
        proyecto.nombre=proyectName
        proyecto.descripcion=proyectDescrption

        # Guarda el objeto en la BD
        proyecto.save()
        messages.success(request, 'Se edito correctamente.')

        return seeProyect(request)
    except:
        return render(request, 'app/404.html')

@login_required
def listdeveloper(request):
    try:
        # Obtiene las proyecto y user
        proyectos = Proyecto.objects.all()
        developers = Developer.objects.all()
        user = User.objects.all()
        tareas = Tarea.objects.all()
        #filtrar datos
        proyectos = proyectos.filter(productowner_id=request.user.id)
        # Crea el contexto
        contexto = {
            'lista_proyecto': proyectos,
            'lista_user': user,
            'lista_developers':developers,
            'active_developer':'active'
        }
        
        return render(request, 'app/listdeveloper.html', contexto)
    except:
        return render(request, 'app/404.html')

@login_required
def view_eliminar_developer(request, id):
    try:
        # Obtiene la pelicula
        developer = Developer.objects.get(pk=id)

        # update la pelicula de la BD
        developer.activo=0
        developer.save()

        return listdeveloper(request)
    except:
        return render(request, 'app/404.html')

@login_required
def view_see_developer(request, id):
    try:
        # Obtiene la pelicula
        avances = Avance.objects.all()
        user = User.objects.all()
        #filtrar datos
        avances = avances.filter(tarea_id=id)

        contexto = {
                'lista_avances':avances,
                'lista_user':user
        }
        return render(request, 'app/seeAvances.html', contexto)
    except:
        return render(request, 'app/404.html')

@login_required
def view_habilitar_developer(request, id):
    try:
        # Obtiene la pelicula
        developer = Developer.objects.get(pk=id)

        # update la pelicula de la BD
        developer.activo=1
        developer.save()

        return listdeveloper(request)
    except:
        return render(request, 'app/404.html')

@login_required
def seeTask(request):
    try:
        # Obtiene las proyecto y user
        proyectos = Proyecto.objects.all()
        proyectosdos = Proyecto.objects.all()
        developers = Developer.objects.all()
        user = User.objects.all()
        tareas = Tarea.objects.all()
        estados = Estado.objects.all()
        #filtrar datos
        proyectos = proyectos.filter(productowner_id=request.user.id)
        developers = developers.filter(activo=True)
        
        #inner join
        
        # Crea el contexto
        contexto = {
            'lista_proyecto': proyectos,
            'lista_proyectosdos':proyectosdos,
            'lista_user': user,
            'lista_developers':developers,
            'lista_tareas': tareas,
            'lista_estados':estados,
            'active_task':'active'
        }
        return render(request, 'app/seeTask.html', contexto)
    except:
        return render(request, 'app/404.html')

def notfound(request):
    return render(request, 'app/404.html')

def autenticar(request):
    try:
        if request.method != 'POST':
            return render(request, 'app/404.html')
        # Obtiene los datos del formulario
        username = request.POST['Username']
        password = request.POST['Password']
        rol = request.POST['CustomSelect']

        request.session['rol_administracion'] = rol
        
        # Obtiene el usuario de la BD
        usuario = authenticate(
            username=username,
            password=password
        )
        # Verifica que el usuario exista
        if usuario is None:
            messages.warning(request, 'Credenciales o cuenta deshabilitada no válidas')
            return render(request, 'app/index.html')
        else:
            # Login en el sistema
            login(request, usuario)
            return redirect('app:menu')
    except:
        return render(request, 'app/404.html')

@login_required
def addTarea(request):
    try:
        if request.method != 'POST':
            return render(request, 'app/404.html')

        # Recupera la info del formulario
        dateNow = datetime.now()
        taskName = request.POST['TaskName']
        taskDescription = request.POST['TaskDescrption']
        taskSelectProyect = request.POST['CustomSelectproyect']
        taskSelectDeveloper = request.POST['CustomSelectdeveloper']
        taskNumber = request.POST['TaskNumber']
        
        # Obtiene la Proyecto y User
        try:
            id_user= User.objects.get(id=taskSelectDeveloper)
        except id_developer.DoesNotExist:
            messages.warning(request, 'El developer no esta asignado al proyecto selectcionado') 
            return seeTask(request)      

        id_proyecto = Proyecto.objects.get(id=taskSelectProyect)
        id_estado = Estado.objects.get(id=1)
        
        #validaciones
        developers = Developer.objects.all()
        developers = developers.filter(Q(usuario_id=id_user) & Q(proyecto=id_proyecto))

        if developers.exists(): 
            # Crea el objeto
            tareas = Tarea(
                nombre=taskName,
                descripcion=taskDescription,
                tiempoestimado=taskNumber,
                fechacreacion=dateNow,
                developer=id_user,
                estado_actual =id_estado,
                proyecto = id_proyecto
            )
            # Guarda el objeto en la BD
            tareas.save()
            messages.success(request, 'Se guardo correctamente.')

            return seeTask(request)
        else:
            messages.warning(request, 'El developer no esta asignado al proyecto selectcionado') 
            return seeTask(request)
    except:
        return render(request, 'app/404.html')

@login_required
def addComent(request):
    try:
        if request.method != 'POST':
            return render(request, 'app/404.html')

        # Recupera la info del formulario
        dateNow = datetime.now()
        id_tarea_id_user = request.POST['prodId']
        taskDescription = request.POST['TaskDescrption']
        tiempotrabjado = request.POST['TaskNumberT']
        tiemporestantes = request.POST['TaskNumberR']

        product = id_tarea_id_user.split(',')    
        # Obtiene la Proyecto y User
        id_tarea = Tarea.objects.get(id=product[0])
        id_user= User.objects.get(id=product[1])
        
        # Crea el objeto
        avance = Avance(
            descripcion=taskDescription,
            fecha=dateNow,
            tiempotrabajado=tiempotrabjado,
            tiemporestante=tiemporestantes,
            tarea=id_tarea,
            usuario=id_user
        )
        # Guarda el objeto en la BD
        avance.save()
        messages.success(request, 'Se guardo correctamente.')

        return seeTask(request)
    except:
        return render(request, 'app/404.html')

@login_required
def EditEstado(request):
    try:
        if request.method != 'POST':
            return render(request, 'app/404.html')

        # Recupera la info del formulario
        estadoid = request.POST['CustomSelectestado']
        tarea = request.POST['prodId']
            
        # Obtiene la User
        Tareaudate = Tarea.objects.get(pk=tarea)
        # Crea el objeto
        Tareaudate.estado_actual_id = estadoid
        # Guarda el objeto en la BD
        Tareaudate.save()
        messages.success(request, 'Se edito correctamente.')
        return seeTask(request)
    except:
        return render(request, 'app/404.html')

@login_required
def EditTask(request):
    try:
        if request.method != 'POST':
            return render(request, 'app/404.html')

        # Recupera la info del formulario
        tarea_id = request.POST['prodId']
        name = request.POST['TaskName']
        descripcion = request.POST['TaskDescrption']
        developer_id = request.POST['CustomSelectdeveloper2']
        number = request.POST['TaskNumber']
        estado_id = request.POST['CustomSelectestado2']
            
        # Obtiene la User
        Tareaudate = Tarea.objects.get(pk=tarea_id)
        # Crea el objeto
        Tareaudate.estado_actual_id = estado_id
        Tareaudate.nombre = name
        Tareaudate.descripcion = descripcion
        Tareaudate.developer_id= developer_id
        Tareaudate.tiempoestimado = number

        # Guarda el objeto en la BD
        Tareaudate.save()
        messages.success(request, 'Se edito correctamente.')
        return seeTask(request)
    except:
        return render(request, 'app/404.html')

@login_required
def view_remove_developer(request, id):
    try:
        # Obtiene la Tarea
        tarea = Tarea.objects.get(pk=id)
        avances = Avance.objects.all()

        #filtrar datos
        avances = avances.filter(usuario_id=id)

        # Borra la Tarea de la BD
        if avances.exists():
            avances.delete()
        
        tarea.delete()

        return seeTask(request)
    except:
        return render(request, 'app/404.html')

def view_logout(request):
    try:
        # Cierra la sesión del usuario
        logout(request)
        # Redirecciona a la pagina de login
        return redirect('app:index')
    except:
        return render(request, 'app/404.html')